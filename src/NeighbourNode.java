import java.net.InetSocketAddress;
import java.util.Objects;

public class NeighbourNode {
    private InetSocketAddress address;
    private InetSocketAddress nextAddress;

    public NeighbourNode(InetSocketAddress address, InetSocketAddress nextAddress) {
        this.address = address;
        this.nextAddress = nextAddress;
    }

    public InetSocketAddress getAddress() {
        return address;
    }

    public void setAddress(InetSocketAddress address) {
        this.address = address;
    }

    public InetSocketAddress getNextAddress() {
        return nextAddress;
    }

    public void setNextAddress(InetSocketAddress nextAddress) {
        this.nextAddress = nextAddress;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NeighbourNode that = (NeighbourNode) o;
        return Objects.equals(address, that.address);
    }

    @Override
    public int hashCode() {
        return Objects.hash(address);
    }
}
