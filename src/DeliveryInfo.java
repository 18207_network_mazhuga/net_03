public class DeliveryInfo {
    private long sentTime;
    private int tries;

    public DeliveryInfo() {
        sentTime = System.currentTimeMillis();
        tries = 1;
    }

    public long getSentTime() {
        return sentTime;
    }

    public int getTries() {
        return tries;
    }

    public void incremntTries() {
        tries++;
        sentTime = System.currentTimeMillis();
    }

    public long getMsecondsFromLastTry() {
        return System.currentTimeMillis() - sentTime;
    }
}
