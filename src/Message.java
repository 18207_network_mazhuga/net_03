import java.net.InetAddress;
import java.util.Objects;
import java.util.UUID;

public class Message {
    private UUID uuid;
    private String text;
    private MessageType messageType;
    private InetAddress destinationAddress;
    private Integer destinationPort;
    private InetAddress fromAddress;
    private Integer fromPort;

    //  destination - это прямой получатель сообщения
    //  from - это тот, кто отправил (не обязательно автор оригинала!)

    //  есть два типа сообщений: отправляемое и полученное
    //  перове подаётся в send, второе получается из read

    //  в отправляемом должны быть destinationPort и destinationAddress, чтобы сокет знал куда слать, и fromPort,
    //  чтобы получатель знал, куда отправить (адрес он знает и так)

    //  в получаемом обязательно должны быть fromAddress и fromPort, чтобы можно было отправить по ним подтверждение

    private Message(UUID uuid, String text, MessageType messageType, InetAddress destinationAddress, Integer destinationPort, InetAddress fromAddress, int fromPort) {
        this.uuid = uuid;
        this.text = text;
        this.messageType = messageType;
        this.destinationAddress = destinationAddress;
        this.destinationPort = destinationPort;
        this.fromAddress = fromAddress;
        this.fromPort = fromPort;
    }

    static public Message createReceived(String stringUUID, String text, int intMessageType,
                                         InetAddress fromAddress, int fromPort) {
        MessageType messageType = null;

        switch (intMessageType) {
            case 0:
                messageType = MessageType.Text;
                break;
            case 1:
                messageType = MessageType.Delivered;
                break;
            case 2:
                messageType = MessageType.Connection;
                break;
            case 3:
                messageType = MessageType.Update;
        }

        return new Message(UUID.fromString(stringUUID), text, messageType, null, null,
                fromAddress, fromPort);

    }

    static public Message createToSend(String text, MessageType messageType, InetAddress destinationAddress,
                                       int destinationPort, int fromPort) {
        return new Message(UUID.randomUUID(), text, messageType, destinationAddress, destinationPort, null, fromPort);
    }

    /*public Message(String text, MessageType messageType, InetAddress destinationAddress, int destinationPort,
                   InetAddress fromAddress, int fromPort) {
        this.uuid = UUID.randomUUID();
        this.text = text;
        this.messageType = messageType;
        this.destinationAddress = destinationAddress;
        this.destinationPort = destinationPort;
        this.fromAddress = fromAddress;
        this.fromPort = fromPort;
    }

    public Message(String stringUUID, String text, int intMessageType, InetAddress destinationAddress, int destinationPort,
                   InetAddress fromAddress, int fromPort) {
        this.uuid = UUID.fromString(stringUUID);
        this.text = text;
        this.destinationAddress = destinationAddress;
        this.destinationPort = destinationPort;
        this.fromAddress = fromAddress;
        this.fromPort = fromPort;

        switch (intMessageType) {
            case 0:
                this.messageType = MessageType.Text;
                break;
            case 1:
                this.messageType = MessageType.Delivered;
                break;
            case 2:
                this.messageType = MessageType.Connection;
                break;
        }
    }*/

    public InetAddress getFromAddress() {
        return fromAddress;
    }

    public int getFromPort() {
        return fromPort;
    }

    public UUID getUUID() {
        return uuid;
    }

    public String getText() {
        return text;
    }

    public MessageType getMessageType() {
        return messageType;
    }

    public InetAddress getDestinationAddress() {
        return destinationAddress;
    }

    public int getDestinationPort() {
        return destinationPort;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return uuid.equals(message.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }
}
