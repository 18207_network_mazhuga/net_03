public class Utility {
    static public byte[] concatenate(byte[] a, byte[] b) {
        byte[] c = new byte[a.length + b.length];
        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);

        return c;
    }

    static public byte[] intToByteArray(int n) {
        byte[] arr = new byte[4];

        arr[0] = ((Integer) (n >>> 24)).byteValue();
        arr[1] = ((Integer) (n >>> 16)).byteValue();
        arr[2] = ((Integer) (n >>> 8)).byteValue();
        arr[3] = ((Integer) (n >>> 0)).byteValue();

        return arr;
    }

}
