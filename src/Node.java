import java.io.*;
import java.net.*;
import java.util.*;

public class Node implements Closeable {
    private final Vector<String> messagesToSend;

    private final int CONNECT_TRIES = 4;
    private final int SEND_TRIES = 2;
    private final int CONFIRMATION_WAIT_MSECONDS = 2000;
    private final int MESSAGE_HISTORY_SIZE = 100;

    private String name;
    private int lossRate;       //from 0 to 100
    private int port;
    private MySocket readSocket;
    private MySocket writeSocket;

    private InetSocketAddress parentNodeAddress;
    private List<NeighbourNode> neighbours;
    private Map<Message, DeliveryInfo> awaitingConfirmation;
    private List<Message> messages;

    private boolean isOnline = true;
    private Random random;

    // конструктор без родителя, 3 аргумента
    public Node(String name, int port, int lossRate, Vector<String> messagesToSend) {
        if (lossRate < 0 || lossRate > 100)
            throw new IllegalArgumentException("lossRate argument should be from 0 to 100, got " + lossRate);

        this.name = name;
        this.lossRate = lossRate;
        this.port = port;

        neighbours = new ArrayList<>();
        parentNodeAddress = null;

        random = new Random();
        awaitingConfirmation = new HashMap<>();
        messages = new ArrayList<>();

        this.messagesToSend = messagesToSend;

        try {
            readSocket = new MySocket(new DatagramSocket(port));
            writeSocket = new MySocket(new DatagramSocket(port + 1));

            readSocket.setSoTimeout(1000);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    // конструктор с родителем, 5 аргументов
    public Node(String name, int port, int lossRate, InetAddress parentAddress, int parentPort, Vector<String> messagesToSend) {
        if (lossRate < 0 || lossRate > 100)
            throw new IllegalArgumentException("lossRate argument should be from 0 to 100, got " + lossRate);

        this.name = name;
        this.lossRate = lossRate;
        this.port = port;

        neighbours = new ArrayList<>();
        parentNodeAddress = new InetSocketAddress(parentAddress, parentPort);

        random = new Random();
        awaitingConfirmation = new HashMap<>();
        messages = new ArrayList<>();

        this.messagesToSend = messagesToSend;

        try {
            readSocket = new MySocket(new DatagramSocket(port));
            writeSocket = new MySocket(new DatagramSocket(port + 1));

            readSocket.setSoTimeout(1000);
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }

    private void connect(InetSocketAddress addressToConnect, boolean asParent) throws IOException {
        for (NeighbourNode neighbourNode : neighbours) {
            if (neighbourNode.getAddress().equals(addressToConnect)) {
                System.out.println("Already connected!");
                return;
            }
        }

        for (int i = 0; i < CONNECT_TRIES; i++) {
            System.out.println("Trying to connect to host (" + addressToConnect.getAddress() + " " + addressToConnect.getPort() + ")...");

            Message message = Message.createToSend("", MessageType.Connection, addressToConnect.getAddress(),
                    addressToConnect.getPort(), port);

            writeSocket.sendMessage(message);
            Message answer;

            try {
                answer = readSocket.readMessage();
                System.out.println("Received answer from host");
            } catch (SocketTimeoutException e) {
                System.out.println("Timeout reached!");
                continue;
            }

            if (answer.getMessageType().equals(MessageType.Delivered)) {
                if (answer.getText().split(" ").length == 2) {
                    String[] nodeInfo = answer.getText().split(" ");

                    InetAddress address = InetAddress.getByName(nodeInfo[0]);
                    int port = Integer.parseInt(nodeInfo[1]);

                    System.out.println("Received parents next node address (" + address + " " + port + ")");
                    neighbours.add(new NeighbourNode(addressToConnect, new InetSocketAddress(address, port)));
                } else {
                    System.out.println("Parent has no next node");
                    neighbours.add(new NeighbourNode(addressToConnect, null));
                }
            }
            System.out.println("Connection successful!");

            if (asParent)
                parentNodeAddress = addressToConnect;

            return;
        }

        System.out.println("Failed to connect");

        if (asParent)
            parentNodeAddress = null;
    }

    private boolean isThereANeighbour(InetSocketAddress address) {
        for (NeighbourNode n : neighbours) {
            if (n.getAddress().equals(address))
                return true;
        }

        return false;
    }

    private void processMessage(Message message) throws IOException {
        if (!isThereANeighbour(new InetSocketAddress(message.getFromAddress(), message.getFromPort())) &&
                message.getMessageType() != MessageType.Connection && message.getMessageType() != MessageType.Update) {
            System.out.println("Message is from unknown source.");
            return;
        }

        switch (message.getMessageType()) {
            case Delivered:
                for (Map.Entry<Message, DeliveryInfo> m : awaitingConfirmation.entrySet()) {

                    // в тексте такого сообщения передаётся uuid того сообщения, чьё получение подтверждается

                    if (m.getKey().getUUID().equals(UUID.fromString(message.getText()))) {
                        awaitingConfirmation.remove(m.getKey());
                        break;
                    }
                }

                break;
            case Text:
                if (messages.contains(message)) {
                    break;
                } else {
                    if (messages.size() >= MESSAGE_HISTORY_SIZE) {
                        messages.remove(0);
                    }
                    messages.add(message);
                }

                System.out.println("Text message: " + message.getText());

                Message confirmation = Message.createToSend(message.getUUID().toString(), MessageType.Delivered,
                        message.getFromAddress(), message.getFromPort(), port);

                writeSocket.sendMessage(confirmation);

                forwardMessage(message);
                break;
            case Connection:
                NeighbourNode newNeighbour = new NeighbourNode(new InetSocketAddress(message.getFromAddress(),
                        message.getFromPort()), null);

                if (neighbours.contains(newNeighbour)) {
                    // если вдруг сообщение пришло повторно
                    break;
                }

                neighbours.add(newNeighbour);

                StringBuilder text = new StringBuilder();
                if (parentNodeAddress != null)
                    text.append(parentNodeAddress.getAddress().toString().replaceFirst("/", ""))
                            .append(" ").append(parentNodeAddress.getPort());

                writeSocket.sendMessage(Message.createToSend(text.toString(), MessageType.Delivered,
                        message.getFromAddress(), message.getFromPort(), port));

                if (neighbours.size() == 2 && parentNodeAddress != null) {
                    // если это первый подключившийся к нам сосед
                    // значит, нужно отправить его адрес родителю для переподключения

                    writeSocket.sendMessage(Message.createToSend(message.getFromAddress().
                                    toString().replaceFirst("/", "") + " " +
                                    message.getFromPort(), MessageType.Update, parentNodeAddress.getAddress(),
                            parentNodeAddress.getPort(), port));
                } else if (parentNodeAddress == null) {
                    // если родителя нет, сделаем этот родительским

                    parentNodeAddress = new InetSocketAddress(message.getFromAddress(), message.getFromPort());

                    for (NeighbourNode n : neighbours) {
                        if (!n.getAddress().equals(parentNodeAddress)) {
                            writeSocket.sendMessage(Message.createToSend(parentNodeAddress.getAddress().
                                            toString().replaceFirst("/", "") + " " +
                                            parentNodeAddress.getPort(), MessageType.Update, n.getAddress().getAddress(),
                                    n.getAddress().getPort(), port));
                        }
                    }
                }

                break;
            case Update:
                // сосед пишет, к какому ноду нужно подключится, если он отключится

                NeighbourNode neighbour = null;
                InetSocketAddress address = new InetSocketAddress(message.getFromAddress(), message.getFromPort());

                // ищем запись соседа, в которой нужно поменять адрес следующего
                for (NeighbourNode n : neighbours) {
                    if (n.getAddress().equals(address)) {
                        neighbour = n;
                        break;
                    }
                }

                if (neighbour == null) {
                    System.err.println("Received update message from someone who is not a neighbour!");
                    return;
                }

                if (message.getText().split(" ").length == 1)
                    neighbour.setNextAddress(null);
                else
                    neighbour.setNextAddress(new InetSocketAddress(
                            InetAddress.getByName(message.getText().split(" ")[0]),
                            Integer.parseInt(message.getText().split(" ")[1])));

                System.out.println("Successfully updated");

                writeSocket.sendMessage(Message.createToSend(message.getUUID().toString(), MessageType.Delivered,
                        message.getFromAddress(), message.getFromPort(), port));
                break;

        }
    }

    private void sendAll(String text, MessageType messageType) throws IOException {
        if (neighbours.size() == 0) {
            System.out.println("No neighbours to send '" + text + "' to!");
            return;
        }

        for (NeighbourNode n : neighbours) {
            Message message = Message.createToSend(text, messageType, n.getAddress().getAddress(),
                    n.getAddress().getPort(), port);

            writeSocket.sendMessage(message);
            awaitingConfirmation.put(message, new DeliveryInfo());
        }

        System.out.println("Sent message '" + text + "' to all neighbours");
    }

    // аналогично прошлой, но не для новых сообщений, а для пересылки дальше

    private void forwardMessage(Message message) throws IOException {
        InetSocketAddress addressToExclude = new InetSocketAddress(message.getFromAddress(), message.getFromPort());

        for (NeighbourNode n : neighbours) {
            if (!n.getAddress().equals(addressToExclude)) {
                Message forwardMessage = Message.createToSend(message.getText(), message.getMessageType(),
                        n.getAddress().getAddress(), n.getAddress().getPort(), port);

                writeSocket.sendMessage(forwardMessage);
                awaitingConfirmation.put(forwardMessage, new DeliveryInfo());
            }
        }

        System.out.println("Forwarded message '" + message.getText() + "' to all neighbours");
    }

    private void checkTimeout() throws IOException {
        ArrayList<Message> toRemove = new ArrayList<>();

        for (Map.Entry<Message, DeliveryInfo> entry : awaitingConfirmation.entrySet()) {
            DeliveryInfo deliveryInfo = entry.getValue();
            Message message = entry.getKey();

            if (deliveryInfo.getTries() >= SEND_TRIES) {
                // если попытки кончились

                System.out.println("Could not send message '" + message.getText() + "', type: " + message.getMessageType() +
                        ", to " + message.getDestinationAddress() + " " + message.getDestinationPort() + " , disconnecting node...");

                InetSocketAddress address = new InetSocketAddress(message.getDestinationAddress(), message.getDestinationPort());

                NeighbourNode neighbourNode = null;
                for (NeighbourNode n : neighbours) {
                    if (n.getAddress().equals(address)) {
                        neighbourNode = n;
                        break;
                    }
                }
                if (neighbourNode == null) {
                    System.err.println("Trying to disconnect someone who is not a neighbour!");
                    return;
                }

                neighbours.remove(neighbourNode);

                // если у удаляемого нода был сосед, делаем его своим соседом
                if (neighbourNode.getNextAddress() != null) {
                    connect(neighbourNode.getNextAddress(), false);
                }

                // если удаляемый был нашим родительским, удаляем его и оттуда
                if (neighbourNode.getAddress().equals(parentNodeAddress))
                    parentNodeAddress = null;

                // обновляем информацию для наших соседей, если их больше одного
                if (neighbours.size() > 1) {
                    if (parentNodeAddress == null) {
                        parentNodeAddress = neighbours.get(0).getAddress();

                        for (NeighbourNode n : neighbours) {
                            if (!n.getAddress().equals(parentNodeAddress)) {
                                writeSocket.sendMessage(Message.createToSend(parentNodeAddress.getAddress().
                                                toString().replaceFirst("/", "") + " " +
                                                parentNodeAddress.getPort(), MessageType.Update, n.getAddress().getAddress(),
                                        n.getAddress().getPort(), port));
                            } else {
                                InetSocketAddress otherNext = neighbours.get(1).getAddress();
                                writeSocket.sendMessage(Message.createToSend(otherNext.getAddress().
                                                toString().replaceFirst("/", "") + " " +
                                                otherNext.getPort(), MessageType.Update, n.getAddress().getAddress(),
                                        n.getAddress().getPort(), port));
                            }
                        }
                    }
                } else if (neighbours.size() == 1) {
                    // если у нас остался один сосед, говорим ему, что переподключаться теперь некуда
                    sendAll(" ", MessageType.Update);
                }

                // удаляем все остальные сообщения для этого нода

                toRemove.add(entry.getKey());
                for (Map.Entry<Message, DeliveryInfo> e : awaitingConfirmation.entrySet()) {
                    if (address.getAddress().equals(e.getKey().getFromAddress())) {
                        toRemove.add(e.getKey());
                    }
                }

                System.out.println("Disconnection of " + message.getDestinationAddress() + " " + message.getDestinationPort() + " successful");

            } else if (deliveryInfo.getMsecondsFromLastTry() > CONFIRMATION_WAIT_MSECONDS) {
                // если время вышло, делаем ещё одну попытку

                System.out.println("Sending message '" + message.getText() + "', type: " + message.getMessageType() +
                        ", to " + message.getDestinationAddress() + " " + message.getDestinationPort() + "' again, try " + (deliveryInfo.getTries() + 1));

                writeSocket.sendMessage(message);
                deliveryInfo.incremntTries();
            }
        }

        for (Message message : toRemove)
            awaitingConfirmation.remove(message);
    }

    private void checkInput() throws IOException {
        ArrayList<String> toDelete = new ArrayList<>();

        for (String text : messagesToSend) {
            sendAll(text, MessageType.Text);
            toDelete.add(text);
        }

        messagesToSend.removeAll(toDelete);
    }

    private void debugPrintNeighbours() {
        System.out.print("Neighbours: ");

        if(neighbours.isEmpty()) {
            System.out.print("empty");
        }

        for (NeighbourNode node : neighbours) {
            System.out.print("(" + node.getAddress().getAddress() + " " + node.getAddress().getPort());

            if (node.getNextAddress() != null) {
                System.out.print(" -> " + node.getNextAddress().getAddress() + " " + node.getNextAddress().getPort());
            }

            System.out.print(") ");
        }

        System.out.println();
    }

    public void start() {
        if (parentNodeAddress != null) {
            try {
                connect(parentNodeAddress, true);
            } catch (IOException e) {
                System.out.println("Error connecting to parent. Starting as lone node.");
            }
        }

        System.out.println("Node " + name + " on port " + port + " started with loss rate " + lossRate + "%");

        while (isOnline) {
            try {
                debugPrintNeighbours();

                try {
                    Message message = readSocket.readMessage();

                    if ((random.nextDouble()) * 100 >= lossRate) {
                        System.out.println("Received message from " + message.getFromAddress() + " " + message.getFromPort()
                                + " of type " + message.getMessageType().toString());
                        processMessage(message);
                    } else
                        System.out.println("Lost message from " + message.getFromAddress() + " " + message.getFromPort()
                                + " of type " + message.getMessageType().toString());
                } catch (SocketTimeoutException ignored) {
                }

                checkInput();

                checkTimeout();
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
        }
    }

    @Override
    public void close() throws IOException {
        readSocket.close();
        writeSocket.close();
    }
}
