import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Vector;

public class InputListenerThread extends Thread{
    private final BufferedReader inputReader = new BufferedReader(new InputStreamReader(System.in));

    private final Vector<String> messagesToSend;

    InputListenerThread(Vector<String> messagesToSend) {
        this.messagesToSend = messagesToSend;
    }

    @Override
    public void run() {
        while(true) {
            try {
                sleep(100);
                if (inputReader.ready()) {
                    String text = inputReader.readLine();
                    messagesToSend.add(text);
                }
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
