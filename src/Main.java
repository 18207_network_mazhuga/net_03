import java.io.IOException;
import java.net.InetAddress;
import java.util.Vector;

class Main {
    public static void main(String[] args) {
        Vector<String> messagesToSend = new Vector<>();

        if (args.length == 5) {
            try (Node node = new Node(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]),
                    InetAddress.getByName(args[3]), Integer.parseInt(args[4]), messagesToSend)) {
                InputListenerThread inputListenerThread = new InputListenerThread(messagesToSend);
                inputListenerThread.start();
                node.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if (args.length == 3) {
            try (Node node = new Node(args[0], Integer.parseInt(args[1]), Integer.parseInt(args[2]), messagesToSend)) {
                InputListenerThread inputListenerThread = new InputListenerThread(messagesToSend);
                inputListenerThread.start();
                node.start();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("3 or 5 arguments needed");
        }
    }
}