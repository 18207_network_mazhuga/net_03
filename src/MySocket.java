import java.io.Closeable;
import java.io.IOException;
import java.math.BigInteger;
import java.net.*;
import java.util.Arrays;

public class MySocket implements Closeable {
    private final int BUFFER_LENGTH = 64 * 1024;
    private DatagramSocket socket;

    public MySocket(DatagramSocket socket) {
        this.socket = socket;
    }

    public Message readMessage() throws IOException {
        DatagramPacket packet = new DatagramPacket(new byte[BUFFER_LENGTH], BUFFER_LENGTH);

        socket.receive(packet);

        /*  Схема пакета:

            UUID                    36 байт     (0-35)
            Тип сообщения           1 байт      (36)
            Порт отправителя        4 байта     (37-40)
            Текст                   оставшиеся байты
         */

        byte[] byteUUID = Arrays.copyOfRange(packet.getData(), 0, 36);
        byte[] byteMessageType = Arrays.copyOfRange(packet.getData(), 36, 37);
        byte[] bytePort = Arrays.copyOfRange(packet.getData(), 37, 41);
        byte[] byteText = Arrays.copyOfRange(packet.getData(), 41, packet.getData().length);

        /*System.out.println("\nReceived message from " + packet.getAddress() + ":" + new BigInteger(bytePort).intValue()
                + " " + Utility.byteArrayToString(packet.getData()));
        System.out.println("UUID: " + new String(byteUUID).trim());
        System.out.println("Text:" + new String(byteText).trim());
        System.out.println("Type: " + new BigInteger(byteMessageType).intValue());*/

        return Message.createReceived(new String(byteUUID).trim(), new String(byteText).trim(),
                new BigInteger(byteMessageType).intValue(), packet.getAddress(), new BigInteger(bytePort).intValue());
    }

    public void sendMessage(Message message) throws IOException {
        byte[] byteUUID = message.getUUID().toString().getBytes();
        byte[] bytePort = Utility.intToByteArray(message.getFromPort());
        byte[] byteText = message.getText().getBytes();

        byte[] byteMessageType = new byte[1];
        byteMessageType[0] = ((Integer) message.getMessageType().ordinal()).byteValue();

        DatagramPacket packet = new DatagramPacket(Utility.concatenate(Utility.concatenate(Utility.concatenate(byteUUID,
                byteMessageType), bytePort), byteText), byteUUID.length + 1 + bytePort.length + byteText.length,
                message.getDestinationAddress(),
                message.getDestinationPort());

        /*System.out.println("\nSending message to " + message.getDestinationAddress() + ":" + message.getDestinationPort()
                + " " + Utility.byteArrayToString(packet.getData()));
        System.out.println("UUID: " + message.getUUID());
        System.out.println("Text:" + message.getText());
        System.out.println("Type: " + message.getMessageType().ordinal());*/

        socket.send(packet);
    }

    public void setSoTimeout(int timeout) throws SocketException {
        socket.setSoTimeout(timeout);
    }

    @Override
    public void close() {
        socket.close();
    }
}
