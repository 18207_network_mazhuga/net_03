public enum MessageType {
    Text, Delivered, Connection, Update
}